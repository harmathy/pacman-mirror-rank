#!/bin/bash

# Copyright (C) 2018 Max Harmathy <max.harmathy@web.de>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

set -e

COUNTRY="$1"

if [ -z "$COUNTRY" ]
then
  echo "usage $0 COUNTRY" >&2
  exit 1
fi

LIST="/etc/pacman.d/mirrorlist"
NEW_LIST="$LIST.pacnew"
BAK_LIST="$LIST.bak"
TMP_LIST="/tmp/mirrorlist.tmp"
RANK_LIST="/tmp/mirrorlist.rank"

if [ -f "$NEW_LIST" ]
then
  mv "$NEW_LIST" "$BAK_LIST"
fi

if ! [ -e "$TMP_LIST" ]
then
  touch "$TMP_LIST"
  grep "## $COUNTRY" "$BAK_LIST" > /dev/null
  awk "/^## ${COUNTRY}\$/{f=1}f==0{next}/^\$/{exit}{print substr(\$0, 1)}" "$BAK_LIST" > "$TMP_LIST"
  sed -i 's/^#Server/Server/' "$TMP_LIST"
  rankmirrors -n 10 "$TMP_LIST" > "$RANK_LIST"
  mv "$RANK_LIST" "$LIST"
  rm -f "$TMP_LIST"
fi

